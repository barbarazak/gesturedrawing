﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Media;
using System.Timers;
using System.Threading;


namespace WindowsFormsApplication1
{
    
    public partial class Form1 : Form
    {
        string s1;
        string fileName;
        string sourcePath;
        string targetPath = @"E:\Obrazki";
        string destFile;
        SystemSound dzwiek = SystemSounds.Exclamation;

        public Form1()
        {
            //sprawdzenie czy istnieje tymczasowy folder
            InitializeComponent();
            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }
            label5.Text = "Gesture Drawing to metoda wykonywania" + Environment.NewLine + "szybkich szkiców, które pomagają" + Environment.NewLine + "rysownikom w poprawieniu swoich" + Environment.NewLine + "umiejętności z postrzegania i szybkiego" + Environment.NewLine + "szkicowania";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                s1 = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(new Action(() =>
            {
                for (int i = 1; i < targetPath.Length; i++)
                {
                    pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                    pictureBox1.Load(Convert.ToString(targetPath + "/obraz (" + i + ").jpg"));
                    //1000 to 1 sekunda
                    System.Threading.Thread.Sleep(30000);
                    dzwiek.Play();
                    if (stop == true)
                    {
                        break;
                    }
                }
                pictureBox1.Image = null;
                stop = false;
            }));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //s1 przechowuje wybrany folder
            sourcePath = s1;
            string[] files = System.IO.Directory.GetFiles(sourcePath);

            //SKOPIOWANIE
            // Copy the files and overwrite destination files if they already exist.
            foreach (string s in files)
            {
                // Use static Path methods to extract only the file name from the path.
                fileName = System.IO.Path.GetFileName(s);
                destFile = System.IO.Path.Combine(targetPath, fileName);
                File.Copy(s, destFile, true);
            }
            MessageBox.Show("Skopiowano");
            //ZMIANA NAZWY
            string[] files2 = System.IO.Directory.GetFiles(targetPath);

            // Use static Path methods to extract only the file name from the path.
            int licznik=1;
            foreach (string s in files2)
            {
                File.Move(targetPath+"/"+System.IO.Path.GetFileName(s), targetPath+"/"+"/obraz ("+licznik+").jpg");
                licznik++;
            }
            MessageBox.Show("zmieniono nazwy");
        }

        private bool stop = false;

        private void button5_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(new Action(() =>
            {
                for (int i = 1; i < targetPath.Length; i++)
                {
                    pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                    pictureBox1.Load(Convert.ToString(targetPath + "/obraz (" + i + ").jpg"));
                    System.Threading.Thread.Sleep(60000);
                    if (stop == true)
                    {
                        break;
                    }
                }
                pictureBox1.Image = null;
                stop = false;
            }));
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(new Action(() =>
            {
                for (int i = 1; i < targetPath.Length; i++)
                {
                    pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                    pictureBox1.Load(Convert.ToString(targetPath + "/obraz (" + i + ").jpg"));
                    System.Threading.Thread.Sleep(120000);
                    if (stop == true)
                    {
                        break;
                    }
                }
                pictureBox1.Image = null;
                stop = false;
            }));
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string[] files2 = System.IO.Directory.GetFiles(targetPath);
            foreach (string s in files2)
            {
                File.Delete(s);
            }
            MessageBox.Show("skasowano pliki");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void button8_MouseMove(object sender, MouseEventArgs e)
        {
        }

        private void button9_Click(object sender, EventArgs e)
        {
            stop = true;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}


